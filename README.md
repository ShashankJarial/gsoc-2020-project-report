# ![ ](assets/atom_electron.svg)  **Icon Customization And Animated Icons**  ![ ](assets/atom_electron.svg)

![ ](assets/Banner.jpeg)

## **Project Description**

EOS-Icons delivered a set of static and animated icons in iconic font having default black colour. While **iconic font** is really efficient for web platforms, installable software struggles more with this solution. For developers working on products like plugins or executable files, it is more efficient to be able to get a .PNG or .SVG of the icon. The default color of the icons in the icon set being black restricted them from being used for projects requiring icons with a different color scheme.

The aim of my GSoC'2020 project is to tackle such problems. A user can now download a single as well as multiple EOS icons in .SVG and .PNG format. I have built a customisation tool and using that, a user can customise the icons delivered by EOS-Icons. Customisations like color, flip, rotate, icon size and icon format are currently supported by this customisation tool. Further I also added a couple of animated icons to the EOS icons set.

Now EOS icons can be used by just anyone in need of an icon be it web platforms or installable softwares. With the customisation tool, our icons can fulfill each and every user's requirements.

<br>

## **Features Added**
1. Single and multiple regular icon direct download in .SVG and .PNG format.

2. Animated icon download in .SVG format.

3. Color customization of icons.(For both, regular and animated icons)

4. Flip and Rotate customisation of icons.(For both, regular and animated icons)

5. Size customization for icons. (Download available in PNG format)

6. Live preview of icons while customization.

7. Unique sharable URL for each icon which enables users to share icons.

8. Added a couple of more animated icons to the set.

A user can customize a single icon or multiple different icons at the same time using select multiple toggle button.

<br>

## **Some Features' Demo**
**1. Icon's Direct .SVG and .PNG Download**

This feature enables user to directly download an icon in their desired format, which can be .SVG or .PNG

![ ](assets/direct-download.gif)

<br>

**2. Regular Icon's Customization**

A user can now customize any regular icon according to thier requirements. They can customize icon color, change icon size(For .PNG download), flip or rotate the icon and select the required icon format (.SVG or .PNG). User can also preview the applied customizations in real time.

![ ](assets/regular-customization.gif)

<br>

**3. Animated Icon's Customization**

A user can now customize any animated icon according to thier requirements. They can customize icon color, flip or rotate the icon. User can also preview the applied customizations in real time.

![ ](assets/animated-customization.gif)

<br>

**4. Multiple Icon's Customization**

This feature enables user to customize multiple icons all together. They can customize icon color, change icon size(For .PNG download), flip or rotate the icon and select the required icon format (.SVG or .PNG). User can also preview the applied customizations in real time for all the icons.

![ ](assets/multiple-customization.gif)

<br>

**5. Sharable URL feature**

This feature enables user to share an icon to any other user. All they need to do is copy the URL and share it with another user. When the URL is followed, the icon for which the URL was shared gets highlighted on the Icons cheatsheet.

![ ](assets/url-sharing.gif)

<br>

## **Contributions**
**EOS-Icon Landing Repository** |**EOS-Icon picker Repository**| Description
-|-|-
[Download Buttons](https://gitlab.com/SUSE-UIUX/eos-icons-landing/-/merge_requests/135) |[.SVG and .PNG Download](https://gitlab.com/SUSE-UIUX/eos-icon-picker/-/merge_requests/18)| Added .SVG and .PNG buttons for direct donwload of EOS icons.
[Single Icon Customization Including Modal](https://gitlab.com/SUSE-UIUX/eos-icons-landing/-/merge_requests/138) <br> [Loading State for Export Button](https://gitlab.com/SUSE-UIUX/eos-icons-landing/-/merge_requests/139)|[Color Customization](https://gitlab.com/SUSE-UIUX/eos-icon-picker/-/merge_requests/16)<br>[Single Icon customization backend](https://gitlab.com/SUSE-UIUX/eos-icon-picker/-/merge_requests/21)| Built Icon Customization Modal and built color customization feature.
[Rotate Feature](https://gitlab.com/SUSE-UIUX/eos-icons-landing/-/merge_requests/141)|[Icon rotation backend](https://gitlab.com/SUSE-UIUX/eos-icon-picker/-/merge_requests/23)| Added rotate feature to the Modal for icon customization.
[Flip Feature](https://gitlab.com/SUSE-UIUX/eos-icons-landing/-/merge_requests/142)|[Icon flip backend](https://gitlab.com/SUSE-UIUX/eos-icon-picker/-/merge_requests/24)| Added flip feature to the Modal for icon customization.
[SVG and PNG dropdown and Size dropdown](https://gitlab.com/SUSE-UIUX/eos-icons-landing/-/merge_requests/142)|[PNG and different size support](https://gitlab.com/SUSE-UIUX/eos-icon-picker/-/merge_requests/25)| Added PNG support for customized icons along with feature to select dfferent sizes for the icon.
[Multiple Icon Customization](https://gitlab.com/SUSE-UIUX/eos-icons-landing/-/merge_requests/146) <br> [Multiple icon Preview](https://gitlab.com/SUSE-UIUX/eos-icons-landing/-/merge_requests/150)|| Added customization support for multiple icons. Enabling users to customize multiple icons all together and preview the changes in real time.
[Animated icon Customization](https://gitlab.com/SUSE-UIUX/eos-icons-landing/-/merge_requests/153)|[Animated icon customization support](https://gitlab.com/SUSE-UIUX/eos-icon-picker/-/merge_requests/29)| Added all the  customization feature for animated icons.
[Sharable URL](https://gitlab.com/SUSE-UIUX/eos-icons-landing/-/merge_requests/155)|| Added URL sharing feature. This feature enables users to share the URL of an icon.
[All merge requests in EOS-icons landing](https://gitlab.com/SUSE-UIUX/eos-icons-landing/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged&author_username=ShashankJarial)|[All merge requests in EOS-icon picker](https://gitlab.com/SUSE-UIUX/eos-icon-picker/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged&author_username=ShashankJarial)| **All other merge requests including these can be found here**

<br>

**EOS-Icon Repository**

Made a few animated icons and they are now available on the [website](https://icons.eosdesignsystem.com/cheatsheet). Below is the link to the Merge Requests for EOS-Icons Repository.
* **[All merge requests in EOS-Icons Repository ](https://gitlab.com/SUSE-UIUX/eos-icons/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged&author_username=ShashankJarial)**

<br>

## **Tech Stack Used**
* NodeJs    
* Express   
* ReactJs
* Sass



<br>

## **Animated Icons**
![](assets/arrow_rotate.svg) ![](assets/atom_electron.svg) ![](assets/bubble_loading.svg) ![](assets/compass.svg) ![](assets/three_dots_loading.svg) ![](assets/hourglass.svg) ![](assets/rotating_gear.svg) ![](assets/typing.svg)

<br>

## **Follow my journey**
As in the tradition of Python software foundation. I have written weekly blogs that discuss in brief about my GSoC journey. Folow this [link](https://blogs.python-gsoc.org/en/shashankjarials-blog/) to check them out.


<br>

## **Thank You !**

